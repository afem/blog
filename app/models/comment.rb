class Comment < ActiveRecord::Base
	belongs_to :post
	# Recordar que por convencion de rails post_id debe ser un foreing a post
	validates_presence_of :post_id
	validates_presence_of :body
end
