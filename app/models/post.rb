class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy

	# V3.5 M9 Aprox
	validates_presence_of :title
	validates_presence_of :body
end
